package com.easy.back.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.back.mapper.HouseInformationMapper;
import com.easy.back.pojo.HouseInformation;
import com.easy.back.pojo.PUser;
import com.easy.back.pojo.dto.HouseInformationDto;
import com.easy.back.service.IHouseInformationService;
import com.easy.back.service.IPUserService;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HouseInformationServiceImpl extends ServiceImpl<HouseInformationMapper, HouseInformation> implements IHouseInformationService {
    @Autowired
    private HouseInformationMapper houseInformationMapper;
    @Autowired
    private IPUserService pUserService;
    @Override
    public HouseInformationDto getById(Long id) {
        HouseInformation houseInformation = houseInformationMapper.selectById(id);

        LambdaQueryWrapper<PUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(PUser::getId, houseInformation.getHouseOwner());
        // 获取平台用户信息
        PUser pUser = pUserService.getOne(wrapper);
        HouseInformationDto houseInformationDto = new HouseInformationDto();
        houseInformationDto.setUserName(pUser.getUserName());
        BeanUtils.copyProperties(houseInformation,houseInformationDto);
        return houseInformationDto;
    }
}
