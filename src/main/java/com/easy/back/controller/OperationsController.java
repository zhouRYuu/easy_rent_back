package com.easy.back.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.easy.back.pojo.Operation;
import com.easy.back.pojo.R;
import com.easy.back.service.IOperationService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 操作日志 控制器
 *
 * @author RuoYu
 * @date 2023/01/01
 */
@RestController
@RequestMapping("operation")
public class OperationsController {

    @Autowired
    private IOperationService operationService;

    @GetMapping("page")
    public R<Page<Operation>> getPage(int page, int pageSize, String name) {
        Page<Operation> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<Operation> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByDesc(Operation::getCreateTime);
        if (name != null) {
            wrapper.and(a -> a.like(Operation::getTarget, name).or().like(Operation::getUser, name));
        }
        operationService.page(pageInfo, wrapper);

/*        if (name != null) {
            List<Operation> records = pageInfo.getRecords();
            List<Operation> collect = records.stream().map((t) -> {
                Operation operation = new Operation();
                BeanUtils.copyProperties(t, operation);
                if (operation.getTarget() != null) {
                    operation.setTarget("<font color='#ff0000'>" + operation.getTarget() + "</front>");
                }
                if (operation.getUser() != null) {
                    operation.setUser("<font color='#ff0000'>" + operation.getUser() + "</front>");
                }
                return operation;
            }).collect(Collectors.toList());
//            System.out.println(collect);
            pageInfo.setRecords(collect);
        }*/
        return R.success(pageInfo);
    }
}
