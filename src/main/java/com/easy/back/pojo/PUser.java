package com.easy.back.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Size;
import java.time.LocalDateTime;

/**
 * 平台用户实体类
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Data
@TableName("user")
public class PUser {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户id唯一标识")
    @TableId("user_id")
    private Long id;

    @ApiModelProperty("用户名")
    private String userName;

    @Size(min = 6, max = 15, message = "请输入6-15位密码")
    @ApiModelProperty("用户密码")
    private String password;

    @Size(min = 11, max = 11, message = "请输入11位手机号")
    @ApiModelProperty("电话")
    private String phone;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("性别 0：男 1：女 2：未知")
    private String sex;

    @ApiModelProperty("身份证号")
    private String idNumber;

    @ApiModelProperty("头像")
    private String avatar;

    @ApiModelProperty("账户状态 0：正常 1：禁用 2：异常")
    private String status;

    @ApiModelProperty("用户创建时间")
    @TableField(fill = FieldFill.INSERT)//插入时填充字段
    private LocalDateTime createTime;

    @ApiModelProperty("账户创建地点和ip")
    @TableField(fill = FieldFill.INSERT)//插入时填充字段
    private String createAddress;

    @ApiModelProperty("登录地点")
    @TableField(fill = FieldFill.INSERT_UPDATE)//插入和更新时填充字段
    private String loginLocation;

    @ApiModelProperty("登陆时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)//插入和更新时填充字段
    private LocalDateTime loginTime;

    @ApiModelProperty("删除标志 0：存在  1：删除")
    private String delFlag;

//    @ApiModelProperty("会员等级 例如（1：v1   2：v2）")
//    private String membersType;

    @ApiModelProperty("备注")
    private String remark;

    @ApiModelProperty("用户类型（0：用户 1：卖家）  默认：0")
    private String type;

    @TableField(exist = false)//表示该属性不为数据库表字段，但又是必须使用的
    private String code;


}
