// 查询列表接口
const getFeedbackPage = (params) => {
  return $axios({
    url: '/feedback/page',
    method: 'get',
    params
  })
}

// 删除当前列的接口
const deleteFeedback = (id) => {
  return $axios({
    url: '/feedback/d',
    method: 'delete',
    params: { id }
  })
}

// 修改接口
const editFeedback = (params) => {
  return $axios({
    url: '/feedback/u',
    method: 'put',
    data: { ...params }
  })
}
