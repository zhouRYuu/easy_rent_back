package com.easy.back.pojo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 反馈实体类
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Data
@TableName("rt_feedback")
public class Feedback implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;//     主键

    private Long userId;// 用户id

    private double sort;// 排序，默认0

    private String type;// 反馈类型

    private LocalDateTime createTime;// 反馈时间

    private LocalDateTime updateTime;//受理时间

    private String content;// 反馈内容

    private String status;// 0：未受理1：已受，理默认0

    //修改人
    private String updateUser;
}
