package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.Order;

/**
 * 订单服务
 *
 * @author RuoYu
 * @date 2023/01/01
 */
public interface IOrderService extends IService<Order> {
}
