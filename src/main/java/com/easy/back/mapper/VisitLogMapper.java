package com.easy.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.back.pojo.VisitLog;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface VisitLogMapper extends BaseMapper<VisitLog> {
}
