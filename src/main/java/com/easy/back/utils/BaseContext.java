package com.easy.back.utils;

/**
 * 基地环境
 * 基础ThreadLocal封装工具类，用于保存和获取当前用户id
 *
 * @author RuoYu
 * @date 2023/01/05
 */
public class BaseContext {
    private static final ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id) {
        threadLocal.set(id);
    }


    public static Long getCurrentId() {
        return threadLocal.get();
    }
    private static final ThreadLocal<Object> a = new ThreadLocal<>();

    public static void set(Object id) {
        a.set(id);
    }


    public static Object get() {
        return a.get();
    }
    private static final ThreadLocal<String> error= new ThreadLocal<>();

    public static void setError(String msg) {
        error.set(msg);
    }


    public static String getError() {
        return error.get();
    }
}
