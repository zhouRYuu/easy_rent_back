package com.easy.back.utils.mq;

import com.easy.back.pojo.EmailBody;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息 生产者
 *
 * @author RuoYu
 * @date 2023/02/07
 */
@Component
public class MessageRocketProducer {
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    public void sendMessage(EmailBody emailBody) {
        System.out.println("待发送短信的订单已纳入消息处理队列(rocketMQ)，id:" + emailBody);
        // 随机生成验证码
        SendCallback callBack = new SendCallback() {
            @Override
            public void onSuccess(SendResult sendResult) {
                System.out.println("消息发送成功");
            }

            @Override
            public void onException(Throwable throwable) {
                System.out.println("消息发送失败！！！！！");
            }
        };
        rocketMQTemplate.asyncSend("email", emailBody, callBack);//发送异步消息
    }

}
