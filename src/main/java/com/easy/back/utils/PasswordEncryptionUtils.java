package com.easy.back.utils;

import org.springframework.util.DigestUtils;

public class PasswordEncryptionUtils {

    public static String encryption(String password) {

        password = DigestUtils.md5DigestAsHex(password.getBytes());

        return password;
    }

}
