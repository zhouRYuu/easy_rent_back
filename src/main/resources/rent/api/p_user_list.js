function getUserList (params) {
  return $axios({
    url: '/p/page',
    method: 'get',
    params
  })
}

// 修改---启用禁用接口
function enableOrDisableEmployee (params) {
  return $axios({
    url: '/p/u',
    method: 'put',
    data: { ...params }
  })
}
// 删除接口
const deleteUser = (id) => {
  return $axios({
    url: '/p/d',
    method: 'delete',
    params: {id}
  })
}