package com.easy.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.back.pojo.PUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PUserMapper extends BaseMapper<PUser> {
}
