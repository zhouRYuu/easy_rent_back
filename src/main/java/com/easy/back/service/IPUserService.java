package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.PUser;

/**
 * 平台用户服务
 *
 * @author RuoYu
 * @date 2023/01/01
 */
public interface IPUserService extends IService<PUser> {
}
