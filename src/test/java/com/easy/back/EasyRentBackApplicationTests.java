package com.easy.back;

import com.easy.back.controller.CommonController;
import com.easy.back.pojo.ErrorLog;
import com.easy.back.service.IErrorLogService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

@SpringBootTest
class EasyRentBackApplicationTests {
    @Value("${redis.house.host}")
    private String host;
    @Autowired
    private CommonController commonController;

    @Test
    void contextLoads() {
//        commonController.delete("f2bae4db-a408-49ad-89a2-c4798da53874.png");
        int[] a = {1, 2};
        int flag = 1;

        for (int i = 0; i < 10; i++)
            System.out.println(flag++);
    }

}
