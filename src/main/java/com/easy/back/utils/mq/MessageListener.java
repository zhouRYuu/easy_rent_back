package com.easy.back.utils.mq;


import com.easy.back.pojo.EmailBody;
import com.easy.back.utils.sendCode.SendMailUtils;
import com.easy.back.utils.sendCode.SendSMSUtils;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 消息消费者
 *
 * @author RuoYu
 * @date 2023/02/07
 */
@Component
@RocketMQMessageListener(topic = "email", consumerGroup = "group_mail")
public class MessageListener implements RocketMQListener<EmailBody> {
    private static final Logger log = LoggerFactory.getLogger(MessageListener.class);

    @Autowired
    private SendMailUtils sendMailUtils;
    @Autowired
    private SendSMSUtils sendSMSUtils;

    @Override
    public void onMessage(EmailBody emailBody) {
        try {
            long l = System.currentTimeMillis();
            if (emailBody.getType().equals("0")) {
                sendSMSUtils.sendMail(emailBody.getSource(), emailBody.getData().toString());
            } else if (emailBody.getType().equals("1")) {
                sendMailUtils.sendErrorMail(emailBody);
            }
            long l2 = System.currentTimeMillis();
            log.info("已完成短信发送业务耗时：" + (l2 - l) / 1000 + "s，----" + emailBody);
        } catch (Exception e) {
            System.out.println("错误");
        }
    }
}
