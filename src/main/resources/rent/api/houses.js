// 查询列表数据
const getHousePage = (params) => {
    return $axios({
        url: '/house/page',
        method: 'get',
        params
    })
}

// 删除数据接口
const deleteHouse = (ids) => {
    return $axios({
        url: '/house/d',
        method: 'delete',
        params: {ids}
    })
}

// 修改数据接口
const editHouse = (params) => {
    return $axios({
        url: '/house/u',
        method: 'put',
        data: {...params}
    })
}

// 新增数据接口
const addSetmeal = (params) => {
    return $axios({
        url: '/house',
        method: 'post',
        data: {...params}
    })
}

// 查询详情接口
const queryHouseById = (id) => {
    return $axios({
        url: `/house/${id}`,
        method: 'get'
    })
}

// 批量起售禁售
const setmealStatusByStatus = (params) => {
    console.log("---" + params.id + "===" + params.status)
    return $axios({
        url: `/house/status/${params.id}/${params.status}`,
        method: 'post'
    })
}
// 批量起售禁售
const deleteOldImg = (name) => {
    return $axios({
        url: `/common/delete/${name}`,
        method: 'delete'
    })
}