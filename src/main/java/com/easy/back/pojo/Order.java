package com.easy.back.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 订单
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Data
@TableName("tb_voucher_order")
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;//  主键

    private Long numberId;//  订单号

    private Long userId;//  下单用户

    private Long rid;//  房屋对象

    private int status;//  订单状态，1：有意向；2：已取消；3：已核销；4：订单完成；

    private LocalDateTime createTime;//  支付时间

    private LocalDateTime refundTime;//  退款时间

    private LocalDateTime updateTime;//  更新时间

}
