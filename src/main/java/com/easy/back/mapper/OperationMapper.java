package com.easy.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.back.pojo.Operation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OperationMapper extends BaseMapper<Operation> {
}
