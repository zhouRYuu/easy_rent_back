function loginCodeApi(data) {
    return $axios({
        'url': '/user/loginCode',
        'method': 'post',
        data
    })
}

function loginApi(data) {
    return $axios({
        'url': '/user/login',
        'method': 'post',
        data
    })
}

function codeApi(phone) {
    console.log(phone + "/////")
    return $axios({
        url: `/user/sendCode/${phone}`,
        method: 'get',
    })
}

function logoutApi() {
    return $axios({
        'url': '/user/logout',
        'method': 'post',
    })
}
