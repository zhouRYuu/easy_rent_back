package com.easy.back.interceptors;

import com.alibaba.fastjson.JSON;
import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CreateCache;
import com.easy.back.pojo.IpVisit;
import com.easy.back.pojo.R;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.TimeUnit;

public class IpInterceptors implements HandlerInterceptor {
    private Cache<String, String> ipCache;

    public IpInterceptors(Cache<String, String> ipCache) {
        this.ipCache = ipCache;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        response.setContentType("text/html;charset=utf-8");
//        System.out.println(request.getRequestURI());///rent/index.html
//        System.out.println(request.getRequestURL());//http://localhost/rent/index.html
        String ip = request.getRemoteAddr() + request.getRequestURL();
        IpVisit ipVisit = (IpVisit) request.getSession().getAttribute(ip);
        if (ipVisit == null) {
            IpVisit iv = new IpVisit();
            iv.setIp(ip);
            iv.setCount(1);
            iv.setFirstTime(System.currentTimeMillis());
            request.getSession().setAttribute(ip, iv);
        } else {
            Long firstTime = ipVisit.getFirstTime();
            if (firstTime == null) {
                response.getWriter().write(JSON.toJSONString(R.error("时间请求为空")));
                return false;
            } else {
                long time = System.currentTimeMillis() - ipVisit.getFirstTime();
                if (time > 5000) {
                    ipVisit.setIp(ip);
                    ipVisit.setCount(1);
                    ipVisit.setFirstTime(System.currentTimeMillis());
                    request.getSession().setAttribute(ip, ipVisit);
                } else {
                    if (ipVisit.getCount() >= 10) {
                        String s = ipCache.get(ip);
                        long times = 0;
                        if (s == null) {
                            ipVisit.setFirstTime(System.currentTimeMillis() + 7000);
                            ipCache.put(ip, "1");
                        }
                        times = ((ipVisit.getFirstTime() - System.currentTimeMillis()) / 1000 + 3);
                        response.getWriter().write(JSON.toJSONString(R.error("请求太快了，" + times + "秒后重试")));
                        return false;
                    } else {
                        ipVisit.setIp(ip);
                        ipVisit.setCount(ipVisit.getCount() + 1);
                        ipVisit.setFirstTime(System.currentTimeMillis());
                        request.getSession().setAttribute(ip, ipVisit);
                    }
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }
}
