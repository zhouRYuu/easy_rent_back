package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.User;

/**
 * 客服用户服务
 *
 * @author RuoYu
 * @date 2023/01/01
 */
public interface IUserService extends IService<User> {
}
