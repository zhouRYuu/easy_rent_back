package com.easy.back.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 房子信息
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Data
@ApiModel("房子信息实体类")
@TableName("tb_house_information")
public class HouseInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键、房屋编码")
    @TableId("id")
    private Long id;

    @ApiModelProperty("创建人 商户id与user表关联")
    private Long houseOwner;

    @ApiModelProperty("分类id")
    private Long categoryId;

    @ApiModelProperty("房屋标题")
    private String title;

    @ApiModelProperty("主页图")
    private String img;

    @ApiModelProperty("房屋价格")
    private double price;

    @ApiModelProperty("房屋类型")
    private String houseType;

    @ApiModelProperty("面积大小")
    private double areaSize;

    @ApiModelProperty("类型：例如 普通住宅/小区房")
    private String type;

    @ApiModelProperty("地址")
    private String location;

    @ApiModelProperty("房源概述")
    private String summary;

    @ApiModelProperty("发布时间")
    private LocalDateTime createTime;

    @ApiModelProperty("更新时间")
    private LocalDateTime updateTime;

    @ApiModelProperty("状态：0：停止 1：启用 2：违规")
    private String status;

    @ApiModelProperty("审核 0：不通过 1：通过")
    private String permissions;

    @ApiModelProperty("删除标志")
    private String deletedFlag;


}

