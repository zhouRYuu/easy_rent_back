package com.easy.back.pojo;

import lombok.Data;

@Data
public class IpVisit {
    private String ip; //用户ip

    private Integer count; //访问次数

    private Long firstTime; //第一次访问时间
}
