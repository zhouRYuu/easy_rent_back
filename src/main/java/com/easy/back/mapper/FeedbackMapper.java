package com.easy.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.back.pojo.Feedback;
import org.apache.ibatis.annotations.Mapper;

/**
 * 反馈映射器
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Mapper
public interface FeedbackMapper extends BaseMapper<Feedback> {
}
