package com.easy.back.pojo.dto;

import com.easy.back.pojo.Feedback;
import lombok.Data;

@Data
public class FeedbackDto extends Feedback {
    private String UserName;
}
