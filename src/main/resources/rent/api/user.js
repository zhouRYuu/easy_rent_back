function getUserList (params) {
  return $axios({
    url: '/user/page',
    method: 'get',
    params
  })
}

// 修改---启用禁用接口
function enableOrDisableEmployee (params) {
  return $axios({
    url: '/user/u',
    method: 'put',
    data: { ...params }
  })
}

// 新增---添加员工
function addEmployee (params) {
  return $axios({
    url: '/user/d',
    method: 'post',
    data: { ...params }
  })
}

// 修改---添加员工
function editEmployee (params) {
  return $axios({
    url: '/user/u',
    method: 'put',
    data: { ...params }
  })
}

// 修改页面反查详情接口
function queryEmployeeById (id) {
  return $axios({
    url: `/user/${id}`,
    method: 'get'
  })
}
// 删除接口
const deleteEmployee = (id) => {
  return $axios({
    url: '/user/r',
    method: 'delete',
    params: {id}
  })
}