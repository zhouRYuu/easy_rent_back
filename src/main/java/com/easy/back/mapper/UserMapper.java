package com.easy.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.back.pojo.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User> {
}
