package com.easy.back.pojo.dto;

import com.easy.back.pojo.HouseInformation;
import lombok.Data;

@Data
public class HouseInformationDto extends HouseInformation {
    private String userName;
}
