// 查询列表数据
const getSystemPage = (params) => {
  return $axios({
    url: '/system/page',
    method: 'get',
    params
  })
}

