package com.easy.back.pojo;

public class EmailBody {
    // 消息id
    private String messageId;
    // 来源email账户
    private String source;
    // 用户名
    private String userName;
    //产生时间
    private String time;
    // ip
    private String ip;
    // 地址
    private String ipAddress;
    // 附加信息
    private Object data;
    // 邮件类型
    private String type;


    public EmailBody() {
    }

    public EmailBody(String type,String messageId, String source, Object data) {
        this.type = type;
        this.messageId = messageId;
        this.source = source;
        this.data = data;
    }

    /**
     * 邮件正文
     *
     * @param type      类型 0:正常邮件 1:错误邮件
     * @param messageId 消息id
     * @param source    源
     * @param userName  用户名
     * @param time      时间
     * @param ip        知识产权
     * @param ipAddress ip地址
     * @param data      数据
     */
    public EmailBody(String type,String messageId, String source, String userName, String time, String ip, String ipAddress, Object data) {
        this.type = type;
        this.messageId = messageId;
        this.source = source;
        this.userName = userName;
        this.time = time;
        this.ip = ip;
        this.ipAddress = ipAddress;
        this.data = data;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "EmailBody{" +
                "messageId='" + messageId + '\'' +
                ", source='" + source + '\'' +
                ", userName='" + userName + '\'' +
                ", time='" + time + '\'' +
                ", ip='" + ip + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", data=" + data +
                ", type='" + type + '\'' +
                '}';
    }
}
