package com.easy.back.utils.ip;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * 获取 ip或地址
 *
 * @author RuoYu
 * @date 2023/01/05
 */
public class IpOrAddress {
    /**
     * 获取 ip
     *
     * @return {@link String}
     */
    public static String getIp() {
        return IpUtils.getIpAddress(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest());
    }

    /**
     * 获取地址
     *
     * @return {@link String}
     */
    public static String getAddress() {
        return AddressUtils.getRealAddressByIP(getIp());
    }
}
