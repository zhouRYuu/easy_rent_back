package com.easy.back.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.back.mapper.PUserMapper;
import com.easy.back.pojo.PUser;
import com.easy.back.service.IPUserService;
import org.springframework.stereotype.Service;

@Service
public class PUserServiceImpl extends ServiceImpl<PUserMapper, PUser> implements IPUserService {
}
