package com.easy.back.pojo;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 错误日志
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Data
public class ErrorLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId("log_id")
    private Long id;// 主键

    private LocalDateTime logTime;// 日志记录时间

    private String errorType;// 错误类型

    private String errorMessage;// 错误信息
    private String errorUri;// 错误路径


}
