package com.easy.back.interceptors;

import com.alibaba.fastjson.JSON;
import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.easy.back.pojo.IpVisit;
import com.easy.back.pojo.Operation;
import com.easy.back.pojo.R;
import com.easy.back.pojo.User;
import com.easy.back.service.IOperationService;
import com.easy.back.service.IUserService;
import com.easy.back.utils.BaseContext;
import com.easy.back.utils.ServletUtils;
import com.easy.back.utils.ip.IpOrAddress;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class OperationInterceptors implements HandlerInterceptor {

    private IOperationService operationService;
    private IUserService userService;

    public OperationInterceptors(IOperationService operationService, IUserService userService) {
        this.operationService = operationService;
        this.userService = userService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        LambdaQueryWrapper<User> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(User::getId, BaseContext.getCurrentId());
        User user = userService.getOne(wrapper);
        String method = request.getMethod();

        Operation operation = new Operation();

        operation.setTarget(type(request.getRequestURI()));//操作对象
        operation.setType(method);//操作类型
        operation.setResourcePath(request.getRequestURI());//资源路径
        operation.setRequestParameter(BaseContext.get().toString());//请求参数
        operation.setUserId(user.getId());//ID
        operation.setUser(user.getUsername());//操作者
        operation.setCreateTime(LocalDateTime.now());//操作时间
        operation.setIpAddress(IpOrAddress.getAddress());//IP地址
        operation.setIp(IpOrAddress.getIp());//地址
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        operation.setBrowser(userAgent.getBrowser().getName());//浏览器类型
        operation.setStatus(user.getStatus() + "");//账户状态
        operation.setError(BaseContext.getError());//异常信息
        operationService.save(operation);
    }

    public String type(String uri) {
        if (uri.contains("feedback/d")) {
            return "反馈处理-删除";
        } else if (uri.contains("feedback/u")) {
            return "反馈处理-受理";
        } else if (uri.contains("house/d")) {
            return "房屋信息审核-删除";
        } else if (uri.contains("house/status")) {
            return "房屋信息审核-状态修改";
        } else if (uri.contains("house/u")) {
            return "房屋信息审核-信息修改";
        } else if (uri.contains("user/u")) {
            return "员工管理-修改";
        } else if (uri.contains("user/d")) {
            return "员工管理-新增";
        } else if (uri.contains("user/r")) {
            return "员工管理-删除";
        } else if (uri.contains("/p/u")) {
            return "平台用户信息-修改";
        } else if (uri.contains("/p/d")) {
            return "平台用户信息-删除";
        }
        return null;
    }
}
