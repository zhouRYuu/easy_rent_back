package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.HouseInformation;
import com.easy.back.pojo.dto.HouseInformationDto;

/**
 * 房子信息服务
 *
 * @author RuoYu
 * @date 2023/01/01
 */
public interface IHouseInformationService extends IService<HouseInformation> {

    public HouseInformationDto getById(Long id);
}
