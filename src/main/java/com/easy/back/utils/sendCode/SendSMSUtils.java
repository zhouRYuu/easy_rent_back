package com.easy.back.utils.sendCode;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


/**
 * smsutils
 *
 * @author RuoYu
 * @date 2023/03/06
 */
@Component
public class SendSMSUtils {
    private static final Logger log = LoggerFactory.getLogger(SendMailUtils.class);

    public void sendMail(String to, String code) {
        //链接阿里云
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI5t8jVMWJ4WecyVxgruv1", "WConU60NMbqv7rT85vlv99DqgdrlIo");
        //构建成客户端
        IAcsClient client = new DefaultAcsClient(profile);
        //构建请求
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");
        //自定义的参数（手机号、验证码、签名、模板）
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PhoneNumbers", to);//手机号码
        request.putQueryParameter("SignName", "suiyizu");//签名名称
        request.putQueryParameter("TemplateCode", "SMS_248235246");//模板的code
        //构建一个短信验证码
        request.putQueryParameter("TemplateParam", "{\"code\":\"" + code + "\"}");

        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            //成功就返回true【response.getHttpResponse().isSuccess()里面默认就带了布尔值】
        } catch (ClientException e) {
            log.error("短信工具类异常信息为:{}", e.getMessage());
        }
    }

    public static void main(String[] args) {
        new SendSMSUtils().sendMail("18777972976", "2131");
    }
}



