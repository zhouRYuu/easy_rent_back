package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.Feedback;

/**
 * 反馈接口层 服务
 *
 * @author RuoYu
 * @date 2022/12/29
 */
public interface IFeedbackService extends IService<Feedback> {
}
