package com.easy.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.back.pojo.HouseInformation;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HouseInformationMapper extends BaseMapper<HouseInformation> {
}
