package com.easy.back.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.back.mapper.VisitLogMapper;
import com.easy.back.pojo.User;
import com.easy.back.pojo.VisitLog;
import com.easy.back.service.IVisitLogService;
import com.easy.back.utils.ip.IpOrAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class VisitLogServiceImpl extends ServiceImpl<VisitLogMapper, VisitLog> implements IVisitLogService {
    @Autowired
    private VisitLogMapper visitLogMapper;

    @Override
    public void addVisitLog(User user) {
        try {
            VisitLog visitLog = new VisitLog();
            visitLog.setUserId(user.getId());
            visitLog.setUserName(user.getUsername());
            visitLog.setName(user.getName());
            visitLog.setLoginIp(IpOrAddress.getIp());
            visitLog.setLoginAdders(IpOrAddress.getAddress());
            visitLog.setLoginTime(LocalDateTime.now());
            visitLog.setStatus(String.valueOf(user.getStatus()));
            visitLogMapper.insert(visitLog);
        } catch (Exception e) {

        }
    }
}
