package com.easy.back.controller;

import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.easy.back.pojo.ErrorLog;
import com.easy.back.pojo.R;
import com.easy.back.pojo.dto.FeedbackDto;
import com.easy.back.service.IErrorLogService;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * 错误日志控制器
 *
 * @author RuoYu
 * @date 2023/01/01
 */
@RestController
@RequestMapping("system")
public class ErrorLogController {
    @Autowired
    private IErrorLogService errorLogService;


    @GetMapping("page")
    public R<Page<ErrorLog>> getPage(int page, int pageSize, String errorType) {

        Page<ErrorLog> pageInfo = new Page<>(page, pageSize);
        LambdaQueryWrapper<ErrorLog> wrapper = new LambdaQueryWrapper<>();

        wrapper.like(Strings.isNotEmpty(errorType), ErrorLog::getErrorType, errorType);
        wrapper.orderByDesc(ErrorLog::getLogTime);

        errorLogService.page(pageInfo, wrapper);

        return R.success(pageInfo);
    }
}
