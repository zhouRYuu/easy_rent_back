package com.easy.back.fillter;

import com.alibaba.fastjson.JSON;
import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CreateCache;
import com.easy.back.pojo.IpVisit;
import com.easy.back.pojo.R;
import com.easy.back.utils.BaseContext;
import com.easy.back.utils.ip.IpOrAddress;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * 检查用户是否已经完成登录过滤器
 *
 * @author RuoYu
 * @date 2023/01/01
 */
@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")
public class LoginCheckFilter implements Filter {
    //路径匹配器，支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();
    @CreateCache(area = "suiyizu", name = "login_ip_", expire = 10, timeUnit = TimeUnit.SECONDS)
    private Cache<String, String> ipCache;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request_ = (HttpServletRequest) request;
        HttpServletResponse response_ = (HttpServletResponse) response;
        //1、获取本次请求的URI
        String requestURI = request_.getRequestURI();
        //定义不需要处理的请求路径
        String[] urls = new String[]{
                "/easy/login",
                "/easy/logout",
                "/rent/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg",
                "/user/login",
                "/user/loginCode",
                "/user/sendCode/**",

                "/doc.html",
                "/webjars/**",
                "/swagger-resources",
                "/v2/api-docs"
        };
        //2、判断本次请求是否需要处理
        boolean check = check(urls, requestURI);

        //3、如果不需要处理，则直接放行
        if (check) {
//            log.info("本次请求{}不需要处理", requestURI);
            chain.doFilter(request_, response_);
            return;
        }
        //4、判断登录状态，如果已登录，则直接放行
        if (request_.getSession().getAttribute("user") != null) {
//            log.info("用户已登录用户ID为{}", request_.getSession().getAttribute("user"));

            Long userId = (Long) request_.getSession().getAttribute("user");
            BaseContext.setCurrentId(userId);

            long id = Thread.currentThread().getId();
//            log.info("线程id为：{}", id);

            chain.doFilter(request_, response_);
            return;
        }
//        log.info("用户未登录");
        //5、如果未登录则返回未登录结果,通过输出流方式向客户端页面响应数据
        response_.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));
        return;
    }

    /**
     * 路径匹配 检查本次请求是否放行
     *
     * @param urls
     * @param requestURI
     * @return
     */
    public boolean check(String[] urls, String requestURI) {
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match) return true;
        }
        return false;
    }
}
