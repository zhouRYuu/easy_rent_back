package com.easy.back.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.easy.back.pojo.Order;
import com.easy.back.pojo.R;
import com.easy.back.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单控制器
 *
 * @author RuoYu
 * @date 2023/01/01
 */
@RestController
@RequestMapping("order")
public class OrderController {
    @Autowired
    private IOrderService orderService;

    @GetMapping("page")
    public R<Page<Order>> page(int page, int pageSize, String number, String beginTime, String endTime) {
        Page<Order> pageInfo = new Page<>(page, pageSize);
        //按条件查询
        LambdaQueryWrapper<Order> lqw = new LambdaQueryWrapper<>();
        lqw.like(number != null, Order::getNumberId, number);//订单号
        lqw.between(beginTime != null, Order::getCreateTime, beginTime, endTime);//时间范围
        lqw.orderByDesc(Order::getUpdateTime);//按照下单时间排序
        orderService.page(pageInfo, lqw);//分页数据
        return R.success(pageInfo);
    }

}
