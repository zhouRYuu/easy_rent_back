package com.easy.back;

import com.alicp.jetcache.anno.config.EnableCreateCacheAnnotation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Slf4j//日志
@SpringBootApplication
@ServletComponentScan//过滤器
@EnableCreateCacheAnnotation
@EnableTransactionManagement//开启事务
public class EasyRentBackApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyRentBackApplication.class, args);
    }

}
