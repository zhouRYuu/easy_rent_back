package com.easy.back.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 访问日志
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Data
@TableName("rt_visit_log")
public class VisitLog {

    private Long id; //  主键
    private Long userId; //  登录id
    private String userName; //  登录账户
    private String name; //  登录用户姓名
    private LocalDateTime loginTime; //  登陆时间
    private String loginAdders; //  登录地点
    private String loginIp; //  登录ip
    private String status;
}

