package com.easy.back.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 操作
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@Data
@TableName("rt_operation_log")
public class Operation implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long id;// 主键
    private String target;// 操作目标
    private String type;// 操作类型
    private String resourcePath;// 资源路径
    private String requestParameter;// 请求参数
    private Long userId;// 操作者id
    private String user;// 操作者账户
    private LocalDateTime createTime;// 操作时间
    private String ipAddress;// ip地址
    private String ip;// ip
    private String browser;// 浏览器类型
    private String status;// 账号状态
    private String error;// 异常信息
}
