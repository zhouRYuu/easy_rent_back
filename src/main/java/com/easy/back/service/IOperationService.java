package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.Operation;

/**
 * 操作日志服务
 *
 * @author RuoYu
 * @date 2023/01/01
 */
public interface IOperationService extends IService<Operation> {
}
