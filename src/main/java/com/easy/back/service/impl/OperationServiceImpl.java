package com.easy.back.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.back.mapper.OperationMapper;
import com.easy.back.pojo.Operation;
import com.easy.back.service.IOperationService;
import org.springframework.stereotype.Service;

@Service
public class OperationServiceImpl extends ServiceImpl<OperationMapper, Operation> implements IOperationService {
}
