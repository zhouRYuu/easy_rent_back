package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.ErrorLog;

/**
 * 错误日志服务
 *
 * @author RuoYu
 * @date 2023/01/01
 */
public interface IErrorLogService extends IService<ErrorLog> {
}
