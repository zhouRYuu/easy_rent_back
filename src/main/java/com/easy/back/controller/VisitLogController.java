package com.easy.back.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.easy.back.pojo.Order;
import com.easy.back.pojo.R;
import com.easy.back.pojo.VisitLog;
import com.easy.back.service.IVisitLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 访问日志控制器
 *
 * @author RuoYu
 * @date 2023/01/01
 */
@RestController
@RequestMapping("/visitLog")
public class VisitLogController {
    @Autowired
    private IVisitLogService visitLogService;


    @GetMapping("/page")
    public R<Page<VisitLog>> getPage(int page, int pageSize, String name, String beginTime, String endTime) {
        System.out.println(page + "--" + pageSize);
        System.out.println(name);
        System.out.println(beginTime + "--" + endTime);
        //分页构造器
        Page<VisitLog> pageInfo = new Page<>(page, pageSize);

        //条件构造器
        LambdaQueryWrapper<VisitLog> lqw = new LambdaQueryWrapper<>();
        // 员工账户 /员工姓名
        lqw.and(name != null, a -> a.like(VisitLog::getName, name).or().like(VisitLog::getUserName, name));
        lqw.between(beginTime != null, VisitLog::getLoginTime, beginTime, endTime);//时间范围
        //添加排序条件，根据sort进行排序
        lqw.orderByDesc(VisitLog::getLoginTime);

        //执行分页查询
        visitLogService.page(pageInfo, lqw);
        return R.success(pageInfo);
    }
}
