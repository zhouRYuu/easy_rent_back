package com.easy.back.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.back.mapper.FeedbackMapper;
import com.easy.back.mapper.UserMapper;
import com.easy.back.pojo.Feedback;
import com.easy.back.pojo.User;
import com.easy.back.service.IFeedbackService;
import com.easy.back.service.IUserService;
import org.springframework.stereotype.Service;

@Service
public class FeedbackServiceImpl extends ServiceImpl<FeedbackMapper, Feedback> implements IFeedbackService {

}

