package com.easy.back.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.back.mapper.ErrorLogMapper;
import com.easy.back.pojo.ErrorLog;
import com.easy.back.service.IErrorLogService;
import org.springframework.stereotype.Service;

@Service
public class ErrorLogServiceImpl extends ServiceImpl<ErrorLogMapper, ErrorLog> implements IErrorLogService {
}
