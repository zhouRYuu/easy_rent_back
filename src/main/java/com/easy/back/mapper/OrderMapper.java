package com.easy.back.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.back.pojo.Order;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface OrderMapper extends BaseMapper<Order> {
}
