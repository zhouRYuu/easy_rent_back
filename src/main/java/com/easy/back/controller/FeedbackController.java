package com.easy.back.controller;

import com.alicp.jetcache.Cache;
import com.alicp.jetcache.anno.CreateCache;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.easy.back.pojo.Feedback;
import com.easy.back.pojo.PUser;
import com.easy.back.pojo.R;
import com.easy.back.pojo.User;
import com.easy.back.pojo.dto.FeedbackDto;
import com.easy.back.service.IFeedbackService;
import com.easy.back.service.IPUserService;
import com.easy.back.service.IUserService;
import com.easy.back.utils.BaseContext;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 反馈控制器
 *
 * @author RuoYu
 * @date 2022/12/29
 */
@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    @Autowired
    private IFeedbackService feedbackService;

    @Autowired
    private IPUserService pusUserService;

    @GetMapping("/page")
    public R<Page<FeedbackDto>> getPage(int page, int pageSize) {
        //分页构造器
        Page<Feedback> pageInfo = new Page<>(page, pageSize);
        Page<FeedbackDto> feedbackDtoPage = new Page<>(page, pageSize);

        //条件构造器
        LambdaQueryWrapper<Feedback> lqw = new LambdaQueryWrapper<>();

        //添加排序条件，根据sort进行排序
        lqw.orderByAsc(Feedback::getSort);

        //执行分页查询
        feedbackService.page(pageInfo, lqw);

        BeanUtils.copyProperties(pageInfo, feedbackDtoPage, "records");
        //3、获取数据
        List<Feedback> records = pageInfo.getRecords();

        List<FeedbackDto> feedbackDtoList = records.stream().map((item) -> {
            FeedbackDto feedbackDto = new FeedbackDto();
            //将数据赋值给dishDto
            BeanUtils.copyProperties(item, feedbackDto);
            if (feedbackDto.getUserId() != null) {
                LambdaQueryWrapper<PUser> wrapper = new LambdaQueryWrapper<>();
                wrapper.eq(PUser::getId, feedbackDto.getUserId());

                // 获取平台用户信息
                PUser pUser = pusUserService.getOne(wrapper);
                feedbackDto.setUserName(pUser.getUserName());
            } else {
                feedbackDto.setUserName("佚名用户");
            }
            return feedbackDto;
        }).collect(Collectors.toList());
        feedbackDtoPage.setRecords(feedbackDtoList);

        return R.success(feedbackDtoPage);
    }


    @DeleteMapping("/d")
    public R<String> delete(Long id) {
        BaseContext.set(id);
        LambdaQueryWrapper<Feedback> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Feedback::getId, id);
        feedbackService.remove(wrapper);
        return R.success("反馈删除成功");
    }

    @Autowired
    private IUserService userService;

    @PutMapping("/u")
    public R<String> update(@RequestBody Feedback category) throws IOException {

        BaseContext.set(category);
        Long id = BaseContext.getCurrentId();
        User user = userService.getById(id);
        String username = user.getUsername();
        category.setUpdateTime(LocalDateTime.now());
        category.setUpdateUser(username);
        category.setStatus("1");
        feedbackService.updateById(category);
        return R.success("修改反馈信息成功");
    }

}
