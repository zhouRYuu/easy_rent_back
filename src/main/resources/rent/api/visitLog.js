// 查询列表接口

function getVisitLogPage (params) {
  return $axios({
    url: '/visitLog/page',
    method: 'get',
    params
  })
}
