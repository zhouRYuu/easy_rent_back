package com.easy.back.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.back.pojo.User;
import com.easy.back.pojo.VisitLog;

/**
 * 访问日志服务
 *
 * @author RuoYu
 * @date 2023/01/01
 */
public interface IVisitLogService extends IService<VisitLog> {
    /**
     * 添加访问日志
     *
     * @param user 用户
     */
    public void addVisitLog(User user);
}
